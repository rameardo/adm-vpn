## Admorris VPN Server

Hier werde ich versuchen, eine einfache theoretische Erklärung dessen zu geben, was ich getan habe!


Das Schutzkonzept im Allgemeinen
----- 

Zunächst einmal müssen wir wissen und zustimmen, dass **kein System 100% sicher ist**. Wir müssen wissen, dass der wahre Schutz in der Person liegt, die das System betreibt, und nicht im System selbst!
Im System gibt es einige Verfahren, die wir versuchen, auf Hacker zu beschränken, um das System technisch zu schützen, aber wenn der Hacker der Person ausnutzt sowie z.B.:(**Social-engineering, phishing, malwares, ransomware etc..**), die dieses System betreibt, dann liegt der Fehler beim Administrator/Person dieses Systems, nicht beim System selbst !
Wir dürfen aber auch nicht leugnen, dass manche Schwachstellen in Systemen oder Programmen meist von Hackern ausgenutzt werden (falls es solche Schwachstellen/Sicherheits gibt)!

**Aus diesem Grund ist ein Sicherheitsbewusstsein bei Admins/Mitarbeitern sehr notwendig**



VPN Server Anforderung
----- 


Dies sind die Mindestanforderungen für einen VPN-Server, Ich empfehle [Hetzner](https://www.hetzner.com/) als anbiter

|  Resource| MIN |
|---|---|
|  RAM | 2GB  |
|  VCPUs oder CPUs | 1  |
| Die restlichen Ressourcen | egal, any |


VPN Server Installation (Linux, Ubuntu dest)
----- 

Meldest du dich per SSH am Server an

```shell
$ ssh sudo@host
```

Lass uns zuerst ein Konto im System erstellen

```shell
$ adduser admorris
```

Befehl ausgabe

```shell
Adding user `admorris' ...
Adding new group `admorris' (1001) ...
Adding new user `admorris' (1001) with group `admorris' ...
Creating home directory `/home/admorris' ...
Copying files from `/etc/skel' ...
New password:
```

Fügen Sie das Konto der SUDO-Gruppe hinzu

```shell
$ adduser admorris sudo
```

Jetzt müssen wir den Server neu starten

```shell
$ reboot
```


Warten wir einige Sekunden, bis es neu gestartet wird, und versuchen wir mit dem neuen Konto anzumelden

```shell
$ ssh admorris@host
```

Lass uns zuerst sehen, ob es ein Update gibt, das wir aktualisieren sollten

```shell
$ sudo apt update && sudo apt upgrade -y
```


**Jetzt beginnen wir mit der Installation und Konfiguration des VPN ([OpenVPN](https://de.wikipedia.org/wiki/OpenVPN) ist ein Open-Source-Projekt)**

Erstellen wir einen Ordner für OpenVPN

```shell
$ mkdir OpenVPN && cd OpenVPN
```

restliche docs für installation kommt noch... 

<!-- 
Jetzt holen wir die [installer](https://gitlab.com/rameardo/adm-vpn/-/blob/main/openvpn.sh) bash file in unsere server __source for configuratino is [angristan](https://github.com/angristan/openvpn-install)

```shell
$ wget https://gitlab.com/rameardo/adm-vpn/-/raw/main/openvpn.sh
```

Jetzt erteilen wir berichtigung `X`'Execute' für datei *openvpn.sh*

```shell
$ chmod 755 openvpn.sh
```

Jetzt führen wir install aus

```shell
$ sudo ./openvpn.sh
```

Folge steps, lass alle einstelleungen einfach default!

-->




